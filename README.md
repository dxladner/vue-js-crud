# vcustomers

> A Vue.js project: A Vue JS CRUD application built using the VUE JS framework which is consuming a test LARAVEL API.
### Info
> The code is written using an example based off of customers but the API is using Stories. So some of the code
> will show customers but the actual data is from stories such as plot, upvotes and writer. You can consume another
> API by just adjusting the custom functions within the Vue Components.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
